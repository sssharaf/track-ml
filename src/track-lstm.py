import numpy as np
import pandas as pd
from trackml.dataset import load_event
import hdbscan
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
import sklearn.preprocessing as pp
from sklearn.cluster import DBSCAN
import sklearn.metrics as metrics
from sklearn.metrics import pairwise
import keras
import keras.backend as K
from keras import layers as KL,models,optimizers


pd.options.display.width=2000

hits, cells, particles, truth = load_event('../input/train_sample/train_100_events/event000001000')

merged = truth.merge(hits,on='hit_id')
merged = merged.set_index('hit_id')
merged = merged.loc[merged.particle_id !=0]

#g_10 = merged.groupby(by='particle_id').count().sort_values(by='x',ascending=False).head()
#m_10 = merged.loc[merged.particle_id.isin(g_10.index)]
m_10 = merged

# Add features
m_10.loc[:,'r'] = np.sqrt(m_10.x**2 + m_10.y**2 )
m_10.loc[:,'d'] = np.sqrt(m_10.x**2 + m_10.y**2 + m_10.z**2 )
m_10.loc[:,'theta'] = np.arctan2(m_10.y,m_10.x)
m_10.loc[:,'phi'] = np.arctan2(m_10.z,m_10.r)

m_10 = m_10.sort_values(by=['particle_id','d'])



MAX_NUM_TRACKS=20
p_map = {}
for p, idx in m_10.groupby(by='particle_id').groups.items():
    v = m_10.loc[idx]
    v = v.loc[:, ['x', 'y', 'z', 'r', 'd', 'theta', 'phi']]
    v = v.as_matrix()
    v = v[:-1]
    v1 = np.zeros(shape=(MAX_NUM_TRACKS, v.shape[1]))
    v1[:v.shape[0]] = v[:]
    p_map[p] = v1

y_map = {}
for p, idx in m_10.groupby(by='particle_id').groups.items():
    v = m_10.loc[idx]
    v = v.loc[:, ['x', 'y', 'z']]
    v = v.as_matrix()
    v = v[1:]
    v1 = np.zeros(shape=(MAX_NUM_TRACKS, v.shape[1]))
    v1[:v.shape[0]] = v[:]
    y_map[p] = v1


### Model

n_a = 40


def mymodel(num_trk,num_f,n_a=20):
    lstm = KL.LSTM(n_a, return_state=True)
    lstm2 = KL.LSTM(n_a, return_state=True)
    densor1 = KL.Dense(20, activation='tanh')
    densor2 = KL.Dense(10, activation='tanh')
    densor3 = KL.Dense(5, activation='tanh')
    densor_out = KL.Dense(3, activation='linear')
    reshapor = KL.Reshape((1, 7))
    masking = KL.Masking(mask_value=0)

    bnorm1 = KL.BatchNormalization()
    bnorm2 = KL.BatchNormalization()
    X = KL.Input(shape=(num_trk,num_f),name='X')
    a0 = KL.Input(shape=(n_a,), name='a0')
    c0 = KL.Input(shape=(n_a,), name='c0')
    a02 = KL.Input(shape=(n_a,), name='a02')
    c02 = KL.Input(shape=(n_a,), name='c02')

    a = a0
    c = c0
    a2 = a02
    c2 = c02
    outputs=[]
    for i in range(0,num_trk):
        x = KL.Lambda(lambda x: x[:,i,:])(X)
        x = reshapor(x)
        x = masking(x)
        x = bnorm1(x)
        a , _,c = lstm(x,initial_state=[a,c])
        ai = KL.Lambda(lambda x: K.expand_dims(x,0))(a)
        a2,_, c2 = lstm2(ai,initial_state=[a2,c2])
        o = densor1(a2)
        o = densor2(o)
        o = bnorm2(o)
        o = densor3(o)
        out = densor_out(o)
        outputs.append(out)

    model = models.Model([X,a0,c0,a02,c02],outputs)
    return model



a0 = np.zeros(shape=(1,n_a,))
c0 = np.zeros(shape=(1,n_a))
a20 = np.zeros(shape=(1,n_a))
c20 = np.zeros(shape=(1,n_a))


p = p_map[734093542489587712]
p = pp.StandardScaler().fit_transform(p)

X = [np.expand_dims(p,0),a0,c0,a20,c20]
y = y_map[734093542489587712]
y= np.expand_dims(y,1)
model = mymodel(MAX_NUM_TRACKS,7,n_a)
adam = optimizers.Adam(lr=5)
model.compile(optimizer=adam,loss='mse',metrics=['mse'])
model.fit(X,list(y),epochs=1,verbose=2)

