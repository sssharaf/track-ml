import numpy as np
import pandas as pd
from trackml.dataset import load_event
import hdbscan
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
import sklearn.preprocessing as pp
from sklearn.cluster import DBSCAN
import sklearn.metrics as metrics
from sklearn.metrics import pairwise
pd.options.display.width=2000

hits, cells, particles, truth = load_event('../input/train_sample/train_100_events/event000001000')

fig = plt.figure()
ax = fig.add_subplot(111,projection='3d')


merged = truth.merge(hits,on='hit_id')
merged = merged.set_index('hit_id')

g_p = merged.loc[merged.particle_id !=0].groupby(by='particle_id')

ax.clear()
ax.set_xlabel('X')
ax.set_ylabel('Y')
ax.set_zlabel('Z')
for i , (k, v) in enumerate(g_p.groups.items()):
    if k ==0:
        continue
    v = merged.iloc[v]
    ax.plot3D(v.x,v.y,v.z,'*')
    if i > 200:
        break


m_8 = merged.query('volume_id == 8 ')
cx = (m_8.loc[m_8.x > 0].x.min() - m_8.loc[m_8.x < 0].x.max() )/2
cy = (m_8.loc[m_8.y > 0].y.min() - m_8.loc[m_8.y < 0].y.max() )/2
cz = (m_8.loc[m_8.z > 0].z.min() - m_8.loc[m_8.z < 0].z.max() )/2
t = np.arctan2(cy,cx)
m_8.loc[:,'m_x'] = m_8.x*np.cos(t) + m_8.y*np.sin(t)
m_8.loc[:,'m_y'] = -m_8.x*np.sin(t) + m_8.y*np.cos(t)
m_8.loc[:,'r'] = np.sqrt(m_8.m_x**2 + m_8.m_y**2 )
m_8.loc[:,'d'] = np.sqrt(m_8.m_x**2 + m_8.m_y**2 + m_8.z**2 )
m_8.loc[:,'theta'] = np.arctan2(m_8.m_y,m_8.m_x)

scaler = pp.StandardScaler()
m_8.loc[:,'s_r'] = scaler.fit_transform(m_8.loc[:,'r'].reshape(-1,1))
m_8.loc[:,'s_theta'] = scaler.fit_transform(m_8.loc[:,'theta'].reshape(-1,1))

g_p_8 = m_8.loc[m_8.particle_id !=0].groupby(by='particle_id')

ax.clear()
ax.set_xlabel('X')
ax.set_ylabel('Y')
ax.set_zlabel('Z')
for i , (k, v) in enumerate(g_p_8.groups.items()):
    if k ==0:
        continue
    v = m_8.loc[v]
    theta = np.arctan2(v.m_y,v.m_x) + i*np.pi
    r = np.sqrt(v.m_x**2 + v.m_y**2 )
    ax.plot3D(theta,r,'*-')
    if i > 200:
        break

def my_metric(p1,p2):
    return metrics.euclidean_distances([p1,p2])
# TEST
#model = hdbscan.HDBSCAN(min_cluster_size=3,min_samples=2,metric='manhattan')
model = DBSCAN(eps=0.001,min_samples=1)
labels = model.fit_predict(m_8.loc[:,['s_theta','s_r']])

li = np.where(np.isin(labels,[1,2,3]))
a = m_8.iloc[li]

ax.clear()
ax.set_xlabel('X')
ax.set_ylabel('Y')
ax.set_zlabel('Z')
ax.scatter(m_8.s_theta,m_8.s_r,c=labels)